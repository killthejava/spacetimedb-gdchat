use godot::prelude::*;

#[derive(GodotClass)]
#[class(base=Node)]
/// The MessageEmitter emits signals triggered from transitions that were originated from reducers
/// It is responsible for letting Godot know that state has changed
pub struct TransitionEmitter {
    #[base]
    base: Base<Node>,
}

#[godot_api]
impl TransitionEmitter {
    #[signal]
    fn chat_has_connected(identity: String);

    #[signal]
    fn chat_has_online_users(online_users: Variant);

    #[signal]
    fn chat_has_disconnected();

    #[signal]
    fn chat_username_was_updated(username: String);

    #[signal]
    fn chat_user_has_joined(username: String);

    #[signal]
    fn chat_remote_user_has_joined(name_or_identity: String);

    #[signal]
    fn chat_remote_user_has_leaved(name_or_identity: String);

    #[signal]
    fn karma_init();

    #[signal]
    fn karma_inc();

    #[signal]
    fn karma_dec();
}

#[godot_api]
impl INode for TransitionEmitter {
    fn init(base: Base<Node>) -> Self {
        Self { base }
    }

    fn ready(&mut self) {
        godot_print!("TransitionEmitter::ready");
    }

    fn exit_tree(&mut self) {
        godot_print!("TransitionEmitter::exit_tree");
    }
}

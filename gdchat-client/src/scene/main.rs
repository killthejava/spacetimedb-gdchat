use godot::engine::Control;
use godot::prelude::*;
use super::payload::controller::PayloadController;
use super::message::emitter::MessageEmitter;
use super::transition::emitter::TransitionEmitter;
use crate::model::runtime::settings::RuntimeSettings;

#[derive(GodotClass)]
#[class(base=Control)]
pub struct MainScene {
    #[export]
    server: GString,
    #[export]
    database: GString,

    controller: Option<Gd<PayloadController>>,
    msg_emitter: Option<Gd<MessageEmitter>>,
    tx_emitter: Option<Gd<TransitionEmitter>>,

    #[base]
    base: Base<Control>,
}

#[godot_api]
impl MainScene {
    /// Returns a tuple containing the server URL and database name
    pub fn get_connection(&self) -> (String, String) {
        (self.server.to_string(), self.database.to_string())
    }
}

#[godot_api]
impl INode for MainScene {
    fn init(base: Base<Control>) -> Self {
        Self {
            server: GString::new(),
            database: GString::new(),
            controller: None,
            tx_emitter: None,
            msg_emitter: None,
            base,
        }
    }

    fn ready(&mut self) {
        godot_print!("MainScene::ready");

        let (controller, tx_emitter, msg_emitter): (
            Option<Gd<PayloadController>>,
            Option<Gd<TransitionEmitter>>,
            Option<Gd<MessageEmitter>>,
        ) = (
            self.base.try_get_node_as("PayloadController"),
            self.base.try_get_node_as("TransitionEmitter"),
            self.base.try_get_node_as("MessageEmitter"),
        );

        match (controller, tx_emitter, msg_emitter) {
            (Some(mut controller), Some(tx_emitter), Some(msg_emitter)) => {
                let settings = RuntimeSettings {
                    controller_id: controller.instance_id(),
                    tx_emitter_id: tx_emitter.instance_id(),
                    msg_emitter_id: msg_emitter.instance_id(),
                };

                controller.bind_mut().setup(settings);

                self.controller = Some(controller);
                self.tx_emitter = Some(tx_emitter);
                self.msg_emitter = Some(msg_emitter);
            }
            _ => {
                godot_error!("MainScene: controller/emitter nodes not found!")
            }
        }
    }
}

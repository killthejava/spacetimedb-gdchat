use std::sync::atomic;
use godot::prelude::*;
use crate::model::runtime::runtime::GodotRuntime;
use crate::model::runtime::settings::RuntimeSettings;
use crate::model::payload::helper::PayloadHelper;
use super::super::main::MainScene;

#[derive(GodotClass)]
#[class(base=Node)]
/// The PayloadController exposes the API for sending payloads to the tokio runtime
pub struct PayloadController {
    runtime: Option<GodotRuntime>,
    counter: atomic::AtomicU32,

    #[base]
    base: Base<Node>,
}

#[godot_api]
impl INode for PayloadController {
    fn init(base: Base<Node>) -> Self {
        Self {
            runtime: None,
            counter: atomic::AtomicU32::new(1),
            base,
        }
    }

    fn ready(&mut self) {
        godot_print!("PayloadController::ready");
    }

    fn exit_tree(&mut self) {
        godot_print!("PayloadController::exit_tree");

        match &self.runtime {
            Some(rt) => rt.send(PayloadHelper::make_teardown()),
            None => {}
        }
    }
}

#[godot_api]
impl PayloadController {
    pub fn setup(&mut self, settings: RuntimeSettings) {
        if let None = self.runtime {
            self.runtime = Some(GodotRuntime::new(settings));
            self.runtime
                .as_ref()
                .unwrap()
                .send(PayloadHelper::make_setup());
        }
    }

    fn next_id(&self) -> u32 {
        self.counter.fetch_add(1, atomic::Ordering::SeqCst)
    }

    #[func]
    pub fn get_next_id(&self) -> Variant {
        Variant::from(self.next_id())
    }

    #[func]
    fn do_connect(&mut self) {
        match self.base.get_parent().unwrap().try_cast::<MainScene>() {
            Ok(parent) => {
                let (server, database) = parent.bind().get_connection();
                let payload = PayloadHelper::make_chat_connect(server, database);
                self.runtime.as_ref().unwrap().send(payload);
            }
            Err(e) => godot_error!("parent node is not MainScene: {}", e.to_string()),
        }
    }

    #[func]
    fn do_disconnect(&mut self) {
        let payload = PayloadHelper::make_chat_disconnect();
        self.runtime.as_ref().unwrap().send(payload);
    }

    #[func]
    fn do_send_message(&mut self, message: String) {
        let payload = PayloadHelper::make_chat_send_message(message);
        self.runtime.as_ref().unwrap().send(payload);
    }

    #[func]
    fn do_send_private_message(&mut self, username: String, message: String) {
        let payload = PayloadHelper::make_chat_send_private_message(username, message);
        self.runtime.as_ref().unwrap().send(payload);
    }
}

use godot::prelude::*;

#[derive(GodotClass)]
#[class(base=Node)]
pub struct MessageEmitter {
    #[base]
    base: Base<Node>,
}

#[godot_api]
/// The MessageEmitter emits signals triggered from messages that were originated from workers
/// It is responsible for letting Godot know that something happened but no state change occurred
impl MessageEmitter {
    #[signal]
    fn chat_connecting();

    #[signal]
    fn chat_connection_failed(err: String);

    #[signal]
    fn chat_disconnecting();

    #[signal]
    fn chat_init_message_list(usernames: VariantArray, messages: VariantArray);

    #[signal]
    fn chat_message_posted(username: String, message: String);

    #[signal]
    fn chat_private_message_received(from: String, message: String);

    #[signal]
    fn karma_received(from: String);
}

#[godot_api]
impl INode for MessageEmitter {
    fn init(base: Base<Node>) -> Self {
        Self { base }
    }

    fn ready(&mut self) {
        godot_print!("MessageEmitter::ready");
    }

    fn exit_tree(&mut self) {
        godot_print!("MessageEmitter::exit_tree");
    }
}

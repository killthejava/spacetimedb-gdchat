use tokio::sync::{mpsc, broadcast};
use godot::prelude::godot_error;
use std::sync::Arc;
use super::super::runtime::settings::RuntimeSettings;
use super::super::message::message::{Message, MessageChannel};
use super::super::message::proxy::MessageProxy;
use super::super::state::manager::StateManager;
use super::super::transition::transition::Transition;
use super::super::transition::dispatcher::{TransitionDispatcher, run_transition_dispatcher};
use super::super::reducer::chat::{ChatReducer, run_chat_reducer};
use super::super::reducer::karma::{KarmaReducer, run_karma_reducer};

pub struct MessageDispatcher {
    pub settings: RuntimeSettings,
    pub sender: mpsc::Sender<Message>,
}

impl MessageDispatcher {
    pub fn new(settings: RuntimeSettings) -> Self {
        let (sender, _recv) = mpsc::channel::<Message>(8);

        Self { settings, sender }
    }

    pub async fn run(&self, mut receiver: mpsc::Receiver<Message>, manager: Arc<StateManager>) {
        let (tx_tx, tx_recv) = mpsc::channel::<Transition>(8);
        let (msg_tx, _msg_recv) = broadcast::channel::<Message>(8);

        let chat_reducer = ChatReducer::new(tx_tx.clone(), manager.chat_state.clone());
        let karma_reducer = KarmaReducer::new(tx_tx.clone(), manager.karma_state.clone());

        let dispatcher = TransitionDispatcher::new(self.settings);

        tokio::spawn(run_chat_reducer(chat_reducer, msg_tx.subscribe()));
        tokio::spawn(run_karma_reducer(karma_reducer, msg_tx.subscribe()));
        tokio::spawn(run_transition_dispatcher(dispatcher, tx_recv));

        let msg_proxy = MessageProxy::new(self.settings.msg_emitter_id);

        while let Some(message) = receiver.recv().await {
            match message.channel {
                MessageChannel::Emit => {
                    msg_proxy.emit_signal(message);
                }
                MessageChannel::Reduce => match msg_tx.send(message) {
                    Ok(_) => {}
                    Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                },
                MessageChannel::Broadcast => {
                    msg_proxy.emit_signal(message.clone());

                    match msg_tx.send(message) {
                        Ok(_) => {}
                        Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                    }
                }
            }
        }
    }
}

pub async fn run_message_dispatcher(
    dispatcher: MessageDispatcher,
    receiver: mpsc::Receiver<Message>,
    manager: Arc<StateManager>,
) {
    dispatcher.run(receiver, manager).await
}

use godot::prelude::{InstanceId, Gd, Node};
use crate::model::runtime::emittable::Emittable;

use super::message::Message;

pub struct MessageProxy {
    msg_emitter_id: InstanceId,
}

impl MessageProxy {
    pub fn new(msg_emitter_id: InstanceId) -> Self {
        Self { msg_emitter_id }
    }

    pub fn emit_signal(&self, message: Message) {
        let mut emitter: Gd<Node> = Gd::from_instance_id(self.msg_emitter_id);
        emitter.call_deferred("emit_signal".into(), &message.varargs());
    }
}

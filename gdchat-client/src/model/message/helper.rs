use spacetimedb_sdk::{
    SubscriptionCallbackId,
    identity::ConnectCallbackId,
    table::{InsertCallbackId, UpdateCallbackId},
    DisconnectCallbackId,
};
use super::message::{Message, MessageType, MessageChannel};
use crate::client;

pub struct MessageHelper {}

impl MessageHelper {
    pub fn make_chat_setup(
        once_on_connect_id: ConnectCallbackId,
        on_connect_id: ConnectCallbackId,
        on_disconnect_id: DisconnectCallbackId,
        on_insert_user: InsertCallbackId<client::User>,
        on_update_user: UpdateCallbackId<client::User>,
        on_insert_message: InsertCallbackId<client::Message>,
        message_subscription_id: SubscriptionCallbackId,
        user_subscription_id: SubscriptionCallbackId,
    ) -> Message {
        Message {
            msg_type: MessageType::ChatSetup {
                once_on_connect_id,
                on_connect_id,
                on_disconnect_id,
                on_insert_user,
                on_update_user,
                on_insert_message,
                message_subscription_id,
                user_subscription_id,
            },
            channel: MessageChannel::Reduce,
        }
    }

    pub fn make_chat_connecting() -> Message {
        Message {
            msg_type: MessageType::ChatConnecting,
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_chat_connection_successful(username: String) -> Message {
        Message {
            msg_type: MessageType::ChatConnectionSuccessful(username),
            channel: MessageChannel::Reduce,
        }
    }

    pub fn make_chat_connection_failed(err: String) -> Message {
        Message {
            msg_type: MessageType::ChatConnectionFailed(err),
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_chat_init_message_list(usernames: Vec<String>, messages: Vec<String>) -> Message {
        Message {
            msg_type: MessageType::ChatInitMessageList {
                usernames,
                messages,
            },
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_chat_disconnecting() -> Message {
        Message {
            msg_type: MessageType::ChatDisconnecting,
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_chat_disconnected() -> Message {
        Message {
            msg_type: MessageType::ChatDisconnected,
            channel: MessageChannel::Reduce,
        }
    }

    pub fn make_chat_username_update_successful(username: String) -> Message {
        Message {
            msg_type: MessageType::ChatUsernameUpdateSuccessful(username),
            channel: MessageChannel::Reduce,
        }
    }

    pub fn make_chat_username_update_failed(err: String) -> Message {
        Message {
            msg_type: MessageType::ChatUsernameUpdateFailed(err),
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_chat_message_send_succesful() -> Message {
        Message {
            msg_type: MessageType::ChatMessageSendSuccessful,
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_chat_message_send_failed(err: String) -> Message {
        Message {
            msg_type: MessageType::ChatMessageSendFailed(err),
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_chat_private_message_sent_successfully(to: String, message: String) -> Message {
        Message {
            msg_type: MessageType::ChatPrivateMessageSentSuccessfully { to, message },
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_chat_private_message_failed_user_missing(to: String) -> Message {
        Message {
            msg_type: MessageType::ChatPrivateMessageFailedUserMissing(to),
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_chat_private_message_failed_user_offline(to: String) -> Message {
        Message {
            msg_type: MessageType::ChatPrivateMessageFailedUserOffline(to),
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_chat_user_renamed(oldname: String, newname: String) -> Message {
        Message {
            msg_type: MessageType::ChatUserRenamed { oldname, newname },
            channel: MessageChannel::Reduce,
        }
    }

    pub fn make_chat_user_joined(username: String) -> Message {
        Message {
            msg_type: MessageType::ChatUserJoined(username),
            channel: MessageChannel::Reduce,
        }
    }

    pub fn make_chat_remote_user_joined(username: String) -> Message {
        Message {
            msg_type: MessageType::ChatRemoteUserJoined(username),
            channel: MessageChannel::Reduce,
        }
    }

    pub fn make_chat_init_online_users(usernames: Vec<String>) -> Message {
        Message {
            msg_type: MessageType::ChatInitOnlineUsers(usernames),
            channel: MessageChannel::Reduce,
        }
    }

    pub fn make_chat_remote_user_leaved(username: String) -> Message {
        Message {
            msg_type: MessageType::ChatRemoteUserLeaved(username),
            channel: MessageChannel::Reduce,
        }
    }

    pub fn make_chat_message_posted(username: String, message: String) -> Message {
        Message {
            msg_type: MessageType::ChatMessagePosted { username, message },
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_chat_private_message_received(username: String, message: String) -> Message {
        Message {
            msg_type: MessageType::ChatPrivateMessageReceived { username, message },
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_karma_setup(
        karma_init_id: SubscriptionCallbackId,
        karma_insert_id: InsertCallbackId<client::Karma>,
    ) -> Message {
        Message {
            msg_type: MessageType::KarmaSetup {
                karma_init_id,
                karma_insert_id,
            },
            channel: MessageChannel::Reduce,
        }
    }

    pub fn make_karma_init_points(total_karma: u32, available_points: u32) -> Message {
        Message {
            msg_type: MessageType::KarmaInitPoints {
                total_karma,
                available_points,
            },
            channel: MessageChannel::Reduce,
        }
    }

    pub fn make_karma_given_successfully(to: String) -> Message {
        Message {
            msg_type: MessageType::KarmaGivenSuccessfully(to),
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_karma_given_failed() -> Message {
        Message {
            msg_type: MessageType::KarmaGivenFailed,
            channel: MessageChannel::Emit,
        }
    }

    pub fn make_karma_received(from: String) -> Message {
        Message {
            msg_type: MessageType::KarmaReceived(from),
            channel: MessageChannel::Reduce,
        }
    }
}

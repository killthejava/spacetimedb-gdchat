use super::super::runtime::emittable::Emittable;
use crate::client;
use godot::prelude::{ToGodot, Variant, VariantArray};
use spacetimedb_sdk::{
    identity::ConnectCallbackId,
    table::{InsertCallbackId, UpdateCallbackId},
    DisconnectCallbackId, SubscriptionCallbackId,
};

#[derive(Clone)]
pub enum MessageType {
    /// Chat callback setup
    ChatSetup {
        once_on_connect_id: ConnectCallbackId,
        on_connect_id: ConnectCallbackId,
        on_disconnect_id: DisconnectCallbackId,
        on_insert_user: InsertCallbackId<client::User>,
        on_update_user: UpdateCallbackId<client::User>,
        on_insert_message: InsertCallbackId<client::Message>,
        message_subscription_id: SubscriptionCallbackId,
        user_subscription_id: SubscriptionCallbackId,
    },

    /// Connection was requested
    ChatConnecting,

    /// Disconnection was requested
    ChatDisconnecting,

    /// Connection was stablished
    ChatConnectionSuccessful(String),

    /// Connection failed
    ChatConnectionFailed(String),

    /// Connection closed
    ChatDisconnected,

    /// Chat messages were retrieved
    ChatInitMessageList {
        usernames: Vec<String>,
        messages: Vec<String>,
    },

    /// Online users were retrieved
    ChatInitOnlineUsers(Vec<String>),

    /// Username updated successfully
    ChatUsernameUpdateSuccessful(String),

    /// Username failed to update
    ChatUsernameUpdateFailed(String),

    /// Message was sent successfully
    ChatMessageSendSuccessful,

    /// Message failed to be sent
    ChatMessageSendFailed(String),

    /// Private message sent succesfully
    ChatPrivateMessageSentSuccessfully { to: String, message: String },

    /// Private message failed: User non existant
    ChatPrivateMessageFailedUserMissing(String),

    /// Private message failed: User not online
    ChatPrivateMessageFailedUserOffline(String),

    /// Remote user change its username
    ChatUserRenamed { oldname: String, newname: String },

    /// Current user joined
    ChatUserJoined(String),

    /// Remote user joined
    ChatRemoteUserJoined(String),

    /// Remote user leaved
    ChatRemoteUserLeaved(String),

    /// Message was posted
    ChatMessagePosted { username: String, message: String },

    /// Private message was received
    ChatPrivateMessageReceived { username: String, message: String },

    /// Karma setup
    KarmaSetup {
        karma_init_id: SubscriptionCallbackId,
        karma_insert_id: InsertCallbackId<client::Karma>,
    },

    /// Initialize karma state
    KarmaInitPoints {
        total_karma: u32,
        available_points: u32,
    },

    /// Karma given succesfully
    KarmaGivenSuccessfully(String),

    /// Karma could not be added
    KarmaGivenFailed,

    /// Remote user gave us karma
    KarmaReceived(String),
}

#[derive(Clone)]
pub enum MessageChannel {
    Emit,
    Reduce,
    Broadcast,
}

impl Default for MessageChannel {
    fn default() -> Self {
        Self::Broadcast
    }
}

#[derive(Clone)]
pub struct Message {
    pub msg_type: MessageType,
    pub channel: MessageChannel,
}

impl Emittable for Message {
    fn signal(&self) -> String {
        match self.msg_type {
            MessageType::ChatSetup { .. } => "chat_setup".into(),
            MessageType::ChatConnecting => "chat_connecting".into(),
            MessageType::ChatDisconnecting => "chat_disconnecting".into(),
            MessageType::ChatConnectionSuccessful(_) => "chat_connection_successful".into(),
            MessageType::ChatConnectionFailed(_) => "chat_connection_failed".into(),
            MessageType::ChatDisconnected => "chat_disconnected".into(),
            MessageType::ChatInitMessageList { .. } => "chat_init_message_list".into(),
            MessageType::ChatInitOnlineUsers(_) => "chat_init_online_users".into(),
            MessageType::ChatUsernameUpdateSuccessful(_) => {
                "chat_username_update_successful".into()
            }
            MessageType::ChatUsernameUpdateFailed(_) => "chat_username_update_failed".into(),
            MessageType::ChatMessageSendSuccessful => "chat_message_send_successful".into(),
            MessageType::ChatMessageSendFailed(_) => "chat_message_send_failed".into(),
            MessageType::ChatPrivateMessageSentSuccessfully { .. } => {
                "chat_private_message_sent_successfully".into()
            }
            MessageType::ChatPrivateMessageFailedUserMissing(_) => {
                "chat_private_message_failed_user_missing".into()
            }
            MessageType::ChatPrivateMessageFailedUserOffline(_) => {
                "chat_private_message_failed_user_offline".into()
            }
            MessageType::ChatUserRenamed { .. } => "chat_user_renamed".into(),
            MessageType::ChatUserJoined(_) => "chat_user_joined".into(),
            MessageType::ChatRemoteUserJoined(_) => "chat_remote_user_joined".into(),
            MessageType::ChatRemoteUserLeaved(_) => "chat_remote_user_leaved".into(),
            MessageType::ChatMessagePosted { .. } => "chat_message_posted".into(),
            MessageType::ChatPrivateMessageReceived { .. } => {
                "chat_private_message_received".into()
            }
            MessageType::KarmaSetup { .. } => "karma_setup".into(),
            MessageType::KarmaInitPoints { .. } => "karma_init_points".into(),
            MessageType::KarmaGivenSuccessfully(_) => "karma_given_successfully".into(),
            MessageType::KarmaGivenFailed => "karma_given_failed".into(),
            MessageType::KarmaReceived(_) => "karma_received".into(),
        }
    }

    fn varargs(&self) -> Vec<Variant> {
        match &self.msg_type {
            MessageType::ChatConnectionFailed(err) => {
                vec![Variant::from(self.signal()), Variant::from(err.as_ref())]
            }
            MessageType::ChatInitMessageList {
                usernames,
                messages,
            } => {
                let usernames: VariantArray =
                    usernames.iter().map(|s| Variant::from(s.clone())).collect();

                let messages: VariantArray =
                    messages.iter().map(|s| Variant::from(s.clone())).collect();

                vec![
                    Variant::from(self.signal()),
                    usernames.to_variant(),
                    messages.to_variant(),
                ]
            }
            MessageType::ChatInitOnlineUsers(usernames) => {
                let usernames: VariantArray =
                    usernames.iter().map(|s| Variant::from(s.clone())).collect();

                vec![Variant::from(self.signal()), usernames.to_variant()]
            }
            MessageType::ChatUsernameUpdateSuccessful(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            MessageType::ChatPrivateMessageSentSuccessfully { to, message } => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(to.as_ref()),
                    Variant::from(message.as_ref()),
                ]
            }
            MessageType::ChatUsernameUpdateFailed(err) => {
                vec![Variant::from(self.signal()), Variant::from(err.as_ref())]
            }
            MessageType::ChatMessageSendFailed(err) => {
                vec![Variant::from(self.signal()), Variant::from(err.as_ref())]
            }
            MessageType::ChatPrivateMessageFailedUserMissing(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            MessageType::ChatPrivateMessageFailedUserOffline(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            MessageType::ChatUserRenamed { oldname, newname } => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(oldname.as_ref()),
                    Variant::from(newname.as_ref()),
                ]
            }
            MessageType::ChatUserJoined(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            MessageType::ChatRemoteUserJoined(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            MessageType::ChatRemoteUserLeaved(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            MessageType::ChatMessagePosted { username, message } => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                    Variant::from(message.as_ref()),
                ]
            }
            MessageType::ChatPrivateMessageReceived { username, message } => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                    Variant::from(message.as_ref()),
                ]
            }
            MessageType::KarmaInitPoints {
                total_karma,
                available_points,
            } => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(*total_karma),
                    Variant::from(*available_points),
                ]
            }
            MessageType::KarmaGivenSuccessfully(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            MessageType::KarmaGivenFailed => {
                vec![Variant::from(self.signal())]
            }
            MessageType::KarmaReceived(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            _ => vec![Variant::from(self.signal())],
        }
    }
}

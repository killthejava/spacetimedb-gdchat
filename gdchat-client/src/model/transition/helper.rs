use super::super::transition::transition::Transition;

pub struct TransitionHelper {}

impl TransitionHelper {
    pub fn make_chat_has_connected(username: String) -> Transition {
        Transition::ChatHasConnected(username)
    }

    pub fn make_chat_got_online_users(usernames: Vec<String>) -> Transition {
        Transition::ChatGotOnlineUsers(usernames)
    }

    pub fn make_chat_username_was_updated(username: String) -> Transition {
        Transition::ChatUsernameWasUpdated(username)
    }

    pub fn make_chat_user_has_been_renamed(oldname: String, newname: String) -> Transition {
        Transition::ChatUserHasBeenRenamed { oldname, newname }
    }

    pub fn make_chat_user_has_joined(username: String) -> Transition {
        Transition::ChatUserHasJoined(username)
    }

    pub fn make_chat_remote_user_has_joined(username: String) -> Transition {
        Transition::ChatRemoteUserHasJoined(username)
    }

    pub fn make_chat_remote_user_has_leaved(username: String) -> Transition {
        Transition::ChatRemoteUserHasLeaved(username)
    }

    pub fn make_chat_has_disconnected() -> Transition {
        Transition::ChatHasDisconnected
    }

    pub fn make_karma_got_initialized(total_karma: u32, available_points: u32) -> Transition {
        Transition::KarmaGotInitialized {
            total_karma,
            available_points,
        }
    }

    pub fn make_karma_has_increased(from: String, total_karma: u32) -> Transition {
        Transition::KarmaHasIncreased { from, total_karma }
    }

    pub fn make_karma_available_points_got_updated(available_points: u32) -> Transition {
        Transition::KarmaAvailablePointsGotUpdated(available_points)
    }
}

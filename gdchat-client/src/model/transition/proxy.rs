use super::super::runtime::emittable::Emittable;
use super::transition::Transition;
use godot::prelude::{Gd, InstanceId, Node};

pub struct TransitionProxy {
    tx_emitter_id: InstanceId,
}

impl TransitionProxy {
    pub fn new(tx_emitter_id: InstanceId) -> Self {
        Self { tx_emitter_id }
    }

    pub fn emit_signal(&self, transition: Transition) {
        let mut emitter: Gd<Node> = Gd::from_instance_id(self.tx_emitter_id);
        emitter.call_deferred("emit_signal".into(), &transition.varargs());
    }
}

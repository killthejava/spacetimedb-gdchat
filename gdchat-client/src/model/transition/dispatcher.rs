use tokio::sync::mpsc;
use super::super::runtime::settings::RuntimeSettings;
use super::proxy::TransitionProxy;
use super::transition::Transition;

pub struct TransitionDispatcher {
    pub settings: RuntimeSettings,
}

impl TransitionDispatcher {
    pub fn new(settings: RuntimeSettings) -> Self {
        Self { settings }
    }

    pub async fn run(&self, mut receiver: mpsc::Receiver<Transition>) {
        let tx_emitter_id = self.settings.tx_emitter_id;
        let proxy = TransitionProxy::new(tx_emitter_id);

        while let Some(transition) = receiver.recv().await {
            proxy.emit_signal(transition.clone());

            // TODO: backpropagator
        }
    }
}

pub async fn run_transition_dispatcher(
    dispatcher: TransitionDispatcher,
    receiver: mpsc::Receiver<Transition>,
) {
    dispatcher.run(receiver).await
}

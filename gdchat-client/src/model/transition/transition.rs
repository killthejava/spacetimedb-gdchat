use super::super::runtime::emittable::Emittable;
use godot::prelude::{ToGodot, Variant, VariantArray};

#[derive(Clone)]
pub enum Transition {
    /// Chat is now connected
    ChatHasConnected(String),

    /// Online users were obtained
    ChatGotOnlineUsers(Vec<String>),

    /// Username was updated
    ChatUsernameWasUpdated(String),

    /// Remote user has changed its username
    ChatUserHasBeenRenamed { oldname: String, newname: String },

    /// User has joined
    ChatUserHasJoined(String),

    /// Remote user has joined
    ChatRemoteUserHasJoined(String),

    /// Remote user has leaved
    ChatRemoteUserHasLeaved(String),

    ///  Chat is now disconnected
    ChatHasDisconnected,

    /// Karma points recovered
    KarmaGotInitialized {
        total_karma: u32,
        available_points: u32,
    },

    /// Karma points were updated
    KarmaHasIncreased { from: String, total_karma: u32 },

    /// Available points were updated
    KarmaAvailablePointsGotUpdated(u32),
}

impl Emittable for Transition {
    fn signal(&self) -> String {
        match self {
            Transition::ChatHasConnected(_) => "chat_has_connected".into(),
            Transition::ChatGotOnlineUsers(_) => "chat_has_online_users".into(),
            Transition::ChatUsernameWasUpdated(_) => "chat_username_was_updated".into(),
            Transition::ChatUserHasBeenRenamed { .. } => "chat_user_has_been_renamed".into(),
            Transition::ChatUserHasJoined(_) => "chat_user_has_joined".into(),
            Transition::ChatRemoteUserHasJoined(_) => "chat_remote_user_has_joined".into(),
            Transition::ChatRemoteUserHasLeaved(_) => "chat_remote_user_has_leaved".into(),
            Transition::ChatHasDisconnected => "chat_has_disconnected".into(),
            Transition::KarmaGotInitialized { .. } => "karma_got_initialized".into(),
            Transition::KarmaHasIncreased { .. } => "karma_has_increased".into(),
            Transition::KarmaAvailablePointsGotUpdated(_) => {
                "karma_available_points_got_updated".into()
            }
        }
    }

    fn varargs(&self) -> Vec<Variant> {
        match self {
            Transition::ChatHasConnected(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            Transition::ChatGotOnlineUsers(online_users) => {
                let online_users =
                    VariantArray::from_iter(online_users.iter().map(|s| Variant::from(s.as_ref())));
                vec![Variant::from(self.signal()), online_users.to_variant()]
            }
            Transition::ChatUsernameWasUpdated(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            Transition::ChatUserHasBeenRenamed { oldname, newname } => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(oldname.as_ref()),
                    Variant::from(newname.as_ref()),
                ]
            }
            Transition::ChatUserHasJoined(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            Transition::ChatRemoteUserHasJoined(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            Transition::ChatRemoteUserHasLeaved(username) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(username.as_ref()),
                ]
            }
            Transition::KarmaGotInitialized {
                total_karma,
                available_points,
            } => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(*total_karma),
                    Variant::from(*available_points),
                ]
            }
            Transition::KarmaHasIncreased { from, total_karma } => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(from.as_ref()),
                    Variant::from(*total_karma),
                ]
            }
            Transition::KarmaAvailablePointsGotUpdated(available_points) => {
                vec![
                    Variant::from(self.signal()),
                    Variant::from(*available_points),
                ]
            }
            _ => vec![Variant::from(self.signal())],
        }
    }
}

use godot::prelude::Variant;

/// The Emittable trait describes the interface for signaling state changes to Godot
pub trait Emittable {
    fn signal(&self) -> String;
    fn varargs(&self) -> Vec<Variant>;
}

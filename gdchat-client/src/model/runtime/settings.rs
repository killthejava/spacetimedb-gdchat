use godot::prelude::InstanceId;

#[derive(Clone, Copy)]
pub struct RuntimeSettings {
    pub controller_id: InstanceId,
    pub tx_emitter_id: InstanceId,
    pub msg_emitter_id: InstanceId,
}

use tokio::sync::mpsc;
use tokio::runtime::Builder;
use godot::prelude::godot_error;
use super::super::payload::payload::Payload;
use super::super::payload::dispatcher::{PayloadDispatcher, run_payload_dispatcher};
use super::settings::RuntimeSettings;

/// The GodotRuntime is responsible for initializing the tokio runtime
pub struct GodotRuntime {
    sender: mpsc::Sender<Payload>,
}

impl GodotRuntime {
    pub fn new(settings: RuntimeSettings) -> Self {
        let rt = Builder::new_multi_thread().enable_all().build().unwrap();
        let (sender, receiver) = mpsc::channel::<Payload>(8);

        let _ = std::thread::spawn(move || {
            rt.block_on(async move {
                let controller = PayloadDispatcher::new(settings);
                run_payload_dispatcher(controller, receiver).await;
            });
        });

        Self { sender }
    }

    pub fn send(&self, payload: Payload) {
        let tx = self.sender.clone();

        tokio::task::block_in_place(move || match tx.blocking_send(payload) {
            Ok(_) => {}
            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
        });
    }
}

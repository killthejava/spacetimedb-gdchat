use super::super::runtime::settings::RuntimeSettings;
use godot::prelude::godot_error;
use std::sync::Arc;
use tokio::sync::{broadcast, mpsc};
use super::payload::Payload;
use super::super::message::message::Message;
use super::super::message::dispatcher::{MessageDispatcher, run_message_dispatcher};
use super::super::state::manager::StateManager;
use super::super::worker::chat::{ChatWorker, run_chat_worker};
use super::super::worker::karma::{KarmaWorker, run_karma_worker};

pub struct PayloadDispatcher {
    pub settings: RuntimeSettings,
}

impl PayloadDispatcher {
    pub fn new(settings: RuntimeSettings) -> Self {
        Self { settings }
    }

    pub async fn run(&self, mut receiver: mpsc::Receiver<Payload>) {
        let (msg_tx, msg_recv) = mpsc::channel::<Message>(8);
        let (pld_tx, pld_recv) = broadcast::channel::<Payload>(8);

        let state_manager = Arc::new(StateManager::new());
        let chat_worker = ChatWorker::new(msg_tx.clone());
        let karma_worker = KarmaWorker::new(msg_tx.clone());
        let message_dispatcher = MessageDispatcher::new(self.settings);

        tokio::spawn(run_chat_worker(
            chat_worker,
            pld_tx.subscribe(),
            state_manager.chat_state.clone(),
        ));

        tokio::spawn(run_karma_worker(
            karma_worker,
            pld_recv,
            state_manager.karma_state.clone(),
        ));

        tokio::spawn(run_message_dispatcher(
            message_dispatcher,
            msg_recv,
            state_manager.clone(),
        ));

        while let Some(payload) = receiver.recv().await {
            match pld_tx.send(payload) {
                Ok(_) => {}
                Err(e) => godot_error!("could not send message: {}", e.to_string()),
            }
        }
    }
}

pub async fn run_payload_dispatcher(
    dispatcher: PayloadDispatcher,
    receiver: mpsc::Receiver<Payload>,
) {
    dispatcher.run(receiver).await
}

use super::payload::Payload;

pub struct PayloadHelper {}

impl PayloadHelper {
    pub fn make_setup() -> Payload {
        Payload::Setup
    }

    pub fn make_teardown() -> Payload {
        Payload::Teardown
    }

    pub fn make_chat_connect(server: String, database: String) -> Payload {
        Payload::ChatConnect { server, database }
    }

    pub fn make_chat_disconnect() -> Payload {
        Payload::ChatDisconnect
    }

    pub fn make_chat_send_message(message: String) -> Payload {
        Payload::ChatSendMessage(message)
    }

    pub fn make_chat_send_private_message(to: String, message: String) -> Payload {
        Payload::ChatSendPrivateMessage { to, message }
    }

    pub fn make_karma_give(to: String) -> Payload {
        Payload::KarmaGive(to)
    }
}

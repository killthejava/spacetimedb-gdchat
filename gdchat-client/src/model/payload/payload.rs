#[derive(Debug, Clone)]
pub enum Payload {
    /// Run initial setup
    Setup,

    /// Free resources
    Teardown,

    /// Connect to server
    ChatConnect { server: String, database: String },

    /// Disconnect from server
    ChatDisconnect,

    /// Send message
    ChatSendMessage(String),

    /// Send private message
    ChatSendPrivateMessage { to: String, message: String },

    /// Give karma to remote user
    KarmaGive(String),
}

use tokio::sync::{mpsc, broadcast};
use std::{
    sync::{Arc, Mutex},
    ops::Sub,
};
use godot::prelude::{godot_error, godot_print};
use spacetimedb_sdk::{
    Address, disconnect,
    identity::{self, on_connect, once_on_connect, Credentials, Identity, remove_on_connect},
    on_disconnect, on_subscription_applied,
    reducer::Status,
    subscribe,
    table::{TableType, TableWithPrimaryKey},
    remove_on_disconnect, remove_on_subscription_applied,
};
use super::super::message::message::Message;
use super::super::message::helper::MessageHelper;
use super::super::payload::payload::Payload;
use super::super::state::chat::ChatState;
use crate::client::{
    self, User, ReducerEvent, set_name, on_set_name, on_send_message, connect, send_message,
};
use crate::chat::command::{get_command, ChatCommand};
use crate::chat::credentials;

pub struct ChatWorker {
    sender: mpsc::Sender<Message>,
}

impl ChatWorker {
    pub fn new(sender: mpsc::Sender<Message>) -> Self {
        Self { sender }
    }

    pub async fn run(
        &self,
        mut receiver: broadcast::Receiver<Payload>,
        state: Arc<Mutex<ChatState>>,
    ) {
        while let Ok(payload) = receiver.recv().await {
            match payload {
                Payload::Setup => {
                    // Save credentials after connection is done
                    let has_credentials = credentials::check_credentials();
                    let once_on_connect_id =
                        once_on_connect(move |creds: &Credentials, _: Address| {
                            if !has_credentials {
                                match credentials::save_credentials(creds) {
                                    Err(_) => {
                                        godot_error!("ChatWorker: failed to save credentials")
                                    }
                                    Ok(_) => godot_print!("ChatWorker: credentials saved"),
                                }
                            }
                        });

                    // Connection callback
                    let conn_tx1 = self.sender.clone();
                    let on_connect_id = on_connect(move |_: &Credentials, _: Address| {
                        let conn_tx1 = conn_tx1.clone();
                        let id = identity::identity().unwrap();
                        let msg = MessageHelper::make_chat_connection_successful(
                            // NOTE: this always return none
                            match client::User::filter_by_identity(id.clone()) {
                                None => identity_leading_hex(&id),
                                Some(u) => user_name_or_identity(&u),
                            },
                        );
                        tokio::task::block_in_place(move || match conn_tx1.blocking_send(msg) {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        });
                    });

                    // User insert callback
                    let conn_tx2 = self.sender.clone();
                    let on_insert_user =
                        User::on_insert(move |user: &User, _: Option<&ReducerEvent>| {
                            if user.online {
                                let msg = MessageHelper::make_chat_user_joined(
                                    user_name_or_identity(user),
                                );
                                let conn_tx2 = conn_tx2.clone();

                                tokio::task::block_in_place(move || {
                                    match conn_tx2.blocking_send(msg) {
                                        Ok(_) => {}
                                        Err(e) => {
                                            godot_error!("receiver was dropped: {}", e.to_string())
                                        }
                                    }
                                });
                            }
                        });

                    // User update callback
                    let conn_tx3 = self.sender.clone();
                    let on_update_user =
                        User::on_update(move |old: &User, new: &User, _: Option<&ReducerEvent>| {
                            let oldname = user_name_or_identity(old);
                            let newname = user_name_or_identity(new);

                            let conn_tx31 = conn_tx3.clone();

                            if old.name != new.name {
                                let msg =
                                    MessageHelper::make_chat_user_renamed(oldname, newname.clone());
                                tokio::task::block_in_place(move || {
                                    match conn_tx31.blocking_send(msg) {
                                        Ok(_) => {}
                                        Err(e) => {
                                            godot_error!("receiver was dropped: {}", e.to_string())
                                        }
                                    }
                                });
                            }

                            let id = identity::identity().unwrap();
                            let msg = match (old.online, new.online) {
                                (true, false) => {
                                    let msg = if new.identity != id {
                                        Some(MessageHelper::make_chat_remote_user_leaved(newname))
                                    } else {
                                        None
                                    };
                                    msg
                                }
                                (false, true) => {
                                    let msg = if new.identity == id {
                                        Some(MessageHelper::make_chat_user_joined(newname))
                                    } else {
                                        Some(MessageHelper::make_chat_remote_user_joined(newname))
                                    };
                                    msg
                                }
                                _ => None,
                            };

                            if let Some(msg) = msg {
                                let conn_tx32 = conn_tx3.clone();

                                tokio::task::block_in_place(move || {
                                    match conn_tx32.blocking_send(msg) {
                                        Ok(_) => {}
                                        Err(e) => {
                                            godot_error!("receiver was dropped: {}", e.to_string())
                                        }
                                    }
                                });
                            }
                        });

                    // Message insert callback
                    let conn_tx4 = self.sender.clone();
                    let on_insert_message = client::Message::on_insert(
                        move |message: &client::Message, reducer_event: Option<&ReducerEvent>| {
                            // ignore if message was posted previously
                            if let None = reducer_event {
                                return;
                            }

                            let id = identity::identity().unwrap();
                            let sender = match User::filter_by_identity(message.sender.clone()) {
                                None => identity_leading_hex(&message.sender),
                                Some(user) => user_name_or_identity(&user),
                            };

                            let msg = match &message.receiver {
                                Some(recv) if *recv == id => {
                                    let msg = MessageHelper::make_chat_private_message_received(
                                        sender,
                                        message.text.clone(),
                                    );
                                    Some(msg)
                                }
                                Some(_) => None,
                                None => {
                                    let msg = MessageHelper::make_chat_message_posted(
                                        sender,
                                        message.text.clone(),
                                    );
                                    Some(msg)
                                }
                            };

                            if let Some(msg) = msg {
                                let conn_tx4 = conn_tx4.clone();
                                tokio::task::block_in_place(move || {
                                    match conn_tx4.blocking_send(msg) {
                                        Ok(_) => {}
                                        Err(e) => {
                                            godot_error!("receiver was dropped: {}", e.to_string())
                                        }
                                    }
                                });
                            }
                        },
                    );

                    // Get list of messages
                    let conn_tx5 = self.sender.clone();

                    let message_subscription_id = on_subscription_applied(move || {
                        let ts: Option<u64> = match std::time::SystemTime::now()
                            .duration_since(std::time::UNIX_EPOCH)
                            .unwrap()
                            .sub(std::time::Duration::from_secs(60 * 15))
                            .as_micros()
                            .try_into()
                        {
                            Ok(v) => Some(v),
                            Err(_) => None,
                        };

                        let mut msgs: Vec<client::Message> =
                            client::Message::filter(|message: &client::Message| {
                                if let Some(ts) = ts {
                                    message.sent >= ts
                                } else {
                                    true
                                }
                            })
                            .collect();
                        msgs.sort_by_key(|m| m.sent);

                        let mut usernames = vec![];
                        let mut messages = vec![];
                        msgs.iter().for_each(|m| {
                            if let Some(user) = User::filter_by_identity(m.sender.clone()) {
                                usernames.push(user_name_or_identity(&user));
                                messages.push(m.text.to_owned());
                            }
                        });

                        let msg = MessageHelper::make_chat_init_message_list(usernames, messages);
                        let conn_tx5 = conn_tx5.clone();
                        tokio::task::block_in_place(move || match conn_tx5.blocking_send(msg) {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        });
                    });

                    // Get online users
                    let conn_tx6 = self.sender.clone();
                    let user_subscription_id = on_subscription_applied(move || {
                        let online_users = client::User::filter_by_online(true)
                            .map(|user| user_name_or_identity(&user))
                            .collect();

                        let msg = MessageHelper::make_chat_init_online_users(online_users);
                        let conn_tx6 = conn_tx6.clone();
                        tokio::task::block_in_place(move || match conn_tx6.blocking_send(msg) {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        });
                    });

                    // Username change callback
                    let conn_tx7 = self.sender.clone();
                    on_set_name(
                        move |_sender_id: &Identity,
                              _sender_address: Option<Address>,
                              status: &Status,
                              name: &String| {
                            let msg = if let Status::Failed(err) = status {
                                MessageHelper::make_chat_username_update_failed(err.to_string())
                            } else {
                                MessageHelper::make_chat_username_update_successful(name.clone())
                            };

                            let conn_tx7 = conn_tx7.clone();
                            tokio::task::block_in_place(move || {
                                match conn_tx7.blocking_send(msg) {
                                    Ok(_) => {}
                                    Err(e) => {
                                        godot_error!("receiver was dropped: {}", e.to_string())
                                    }
                                }
                            });
                        },
                    );

                    // Message send callback
                    let conn_tx8 = self.sender.clone();
                    on_send_message(
                        move |_sender_id: &Identity,
                              _sender_address: Option<Address>,
                              status: &Status,
                              _text: &String,
                              _receiver: &Option<Identity>| {
                            let msg = if let Status::Failed(err) = status {
                                MessageHelper::make_chat_message_send_failed(err.clone())
                            } else {
                                MessageHelper::make_chat_message_send_succesful()
                            };

                            let conn_tx8 = conn_tx8.clone();
                            tokio::task::block_in_place(move || {
                                match conn_tx8.blocking_send(msg) {
                                    Ok(_) => {}
                                    Err(e) => {
                                        godot_error!("receiver was dropped: {}", e.to_string())
                                    }
                                }
                            });
                        },
                    );

                    // Disconnection callback
                    let conn_tx9 = self.sender.clone();
                    let on_disconnect_id = on_disconnect(move || {
                        let msg = MessageHelper::make_chat_disconnected();
                        let conn_tx9 = conn_tx9.clone();
                        tokio::task::block_in_place(move || match conn_tx9.blocking_send(msg) {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        });
                    });

                    // Chat setup message
                    let msg = MessageHelper::make_chat_setup(
                        once_on_connect_id,
                        on_connect_id,
                        on_disconnect_id,
                        on_insert_user,
                        on_update_user,
                        on_insert_message,
                        message_subscription_id,
                        user_subscription_id,
                    );
                    tokio::task::block_in_place(move || {
                        match self.sender.clone().blocking_send(msg) {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                    });
                }
                Payload::ChatConnect { server, database } => {
                    // check if we're already connected
                    let is_connected = {
                        match state.try_lock() {
                            Ok(state) => state.connected,
                            Err(e) => {
                                godot_error!("unable to read chat state: {}", e.to_string());
                                false
                            }
                        }
                    };

                    if is_connected {
                        continue;
                    }

                    let msg = MessageHelper::make_chat_connecting();
                    match self.sender.send(msg).await {
                        Ok(_) => {}
                        Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                    }

                    let creds = match credentials::load_credentials() {
                        Err(e) => {
                            godot_error!("ChatWorker: could not read credentials: {}", e);
                            None
                        }
                        Ok(None) => {
                            godot_print!("ChatWorker: no credentials found");
                            None
                        }
                        Ok(Some(c)) => Some(c),
                    };

                    match connect(server, &database, creds) {
                        Ok(_) => {
                            subscribe(&[
                                "SELECT * FROM User;",
                                "SELECT * FROM Message;",
                                "SELECT * FROM Karma;",
                            ])
                            .unwrap();
                        }
                        Err(e) => {
                            let msg = MessageHelper::make_chat_connection_failed(e.to_string());
                            match self.sender.send(msg).await {
                                Ok(_) => {}
                                Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                            }
                        }
                    }
                }
                Payload::ChatSendMessage(content) => match get_command(content.as_ref()) {
                    Some(ChatCommand::ChangeUsername(username)) => set_name(username),
                    None => send_message(content.to_string(), None),
                },
                Payload::ChatSendPrivateMessage { to, message } => {
                    let user: Vec<_> = User::filter_by_name(Some(to.clone())).collect();
                    if user.len() == 0 {
                        let msg = MessageHelper::make_chat_private_message_failed_user_missing(to);
                        match self.sender.send(msg).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                        continue;
                    }

                    let user = user.first().unwrap();
                    if !user.online {
                        let msg = MessageHelper::make_chat_private_message_failed_user_offline(to);
                        match self.sender.send(msg).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                        continue;
                    }

                    send_message(message, Some(user.identity.clone()));
                }
                Payload::ChatDisconnect => {
                    let is_connected = {
                        match state.try_lock() {
                            Ok(state) => state.connected,
                            Err(e) => {
                                godot_error!("unable to read chat state: {}", e.to_string());
                                false
                            }
                        }
                    };

                    if !is_connected {
                        continue;
                    }

                    let msg = MessageHelper::make_chat_disconnecting();
                    match self.sender.send(msg).await {
                        Ok(_) => {}
                        Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                    }
                    disconnect();
                }
                Payload::Teardown => {
                    let is_connected = match state.try_lock() {
                        Err(e) => {
                            godot_error!("unable to read chat state: {}", e.to_string());
                            false
                        }
                        Ok(mut v) => {
                            if v.connected {
                                remove_on_connect(v.once_on_connect_id.take().unwrap());
                                remove_on_connect(v.on_connect_id.take().unwrap());
                                remove_on_disconnect(v.on_disconnect_id.take().unwrap());
                                client::User::remove_on_insert(v.on_insert_user.take().unwrap());
                                client::User::remove_on_update(v.on_update_user.take().unwrap());
                                client::Message::remove_on_insert(
                                    v.on_insert_message.take().unwrap(),
                                );
                                remove_on_subscription_applied(
                                    v.message_subscription_id.take().unwrap(),
                                );
                                remove_on_subscription_applied(
                                    v.user_subscription_id.take().unwrap(),
                                );
                            }
                            v.connected
                        }
                    };

                    if is_connected {
                        godot_print!("ChatWorker: disconnecting from server");
                        disconnect();
                    }
                }
                _ => {}
            }
        }
    }
}

pub async fn run_chat_worker(
    worker: ChatWorker,
    receiver: broadcast::Receiver<Payload>,
    state: Arc<Mutex<ChatState>>,
) {
    worker.run(receiver, state).await
}

fn user_name_or_identity(user: &User) -> String {
    user.name
        .clone()
        .unwrap_or_else(|| identity_leading_hex(&user.identity))
}

fn identity_leading_hex(id: &Identity) -> String {
    hex::encode(&id.bytes()[0..8])
}

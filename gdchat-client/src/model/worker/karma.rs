use tokio::sync::{mpsc, broadcast};
use std::sync::{Arc, Mutex};
use godot::prelude::{godot_error, godot_print};
use spacetimedb_sdk::{
    on_subscription_applied, identity, table::TableType, remove_on_subscription_applied,
};
use super::super::message::message::Message;
use super::super::message::helper::MessageHelper;
use super::super::payload::payload::Payload;
use super::super::state::karma::KarmaState;
use crate::client::{self, Karma, give_karma, User, ReducerEvent};

pub struct KarmaWorker {
    sender: mpsc::Sender<Message>,
}

impl KarmaWorker {
    pub fn new(sender: mpsc::Sender<Message>) -> Self {
        Self { sender }
    }

    pub async fn run(
        &self,
        mut receiver: broadcast::Receiver<Payload>,
        state: Arc<Mutex<KarmaState>>,
    ) {
        while let Ok(payload) = receiver.recv().await {
            match payload {
                Payload::Setup => {
                    let sub_tx = self.sender.clone();
                    let karma_init_id = on_subscription_applied(move || {
                        let karma = match identity::identity() {
                            Ok(id) => Some(client::Karma::filter_by_receiver(id)),
                            Err(e) => {
                                godot_error!("could not obtain identity: {}", e.to_string());
                                None
                            }
                        };

                        let sub_tx = sub_tx.clone();
                        if let Some(karma) = karma {
                            let user =
                                client::User::filter_by_identity(identity::identity().unwrap());
                            let available_points = user.unwrap().available_points;

                            let msg = MessageHelper::make_karma_init_points(
                                karma.count() as u32,
                                available_points,
                            );

                            tokio::task::block_in_place(move || match sub_tx.blocking_send(msg) {
                                Ok(_) => {}
                                Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                            });
                        }
                    });

                    let insert_tx = self.sender.clone();
                    let karma_insert_id =
                        Karma::on_insert(move |karma: &Karma, _: Option<&ReducerEvent>| {
                            let id = identity::identity().unwrap();
                            match (&karma.sender, &karma.receiver) {
                                (sender, receiver) => {
                                    if *sender == id {
                                        let receiver =
                                            User::filter_by_identity(receiver.clone()).unwrap();
                                        let msg = MessageHelper::make_karma_given_successfully(
                                            receiver.name.clone().unwrap(),
                                        );
                                        let insert_tx = insert_tx.clone();
                                        tokio::task::block_in_place(move || {
                                            match insert_tx.blocking_send(msg) {
                                                Ok(_) => {}
                                                Err(e) => godot_error!(
                                                    "receiver was dropped: {}",
                                                    e.to_string()
                                                ),
                                            }
                                        });
                                    } else if *receiver == id {
                                        let sender =
                                            User::filter_by_identity(sender.clone()).unwrap();
                                        let msg = MessageHelper::make_karma_received(
                                            sender.name.clone().unwrap(),
                                        );
                                        let insert_tx = insert_tx.clone();
                                        tokio::task::block_in_place(move || {
                                            match insert_tx.blocking_send(msg) {
                                                Ok(_) => {}
                                                Err(e) => godot_error!(
                                                    "receiver was dropped: {}",
                                                    e.to_string()
                                                ),
                                            }
                                        });
                                    }
                                }
                            }
                        });

                    let sub_tx = self.sender.clone();
                    let msg = MessageHelper::make_karma_setup(karma_init_id, karma_insert_id);
                    match sub_tx.send(msg).await {
                        Ok(_) => {}
                        Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                    };
                }
                Payload::KarmaGive(username) => {
                    let user: Vec<_> = User::filter_by_name(Some(username)).collect();
                    if user.len() == 0 {
                        let msg = MessageHelper::make_karma_given_failed();
                        let sender = self.sender.clone();
                        match sender.send(msg).await {
                            Ok(_) => {}
                            Err(err) => godot_error!("receiver was dropped: {}", err.to_string()),
                        }
                        continue;
                    }

                    let has_points = match state.try_lock() {
                        Ok(state) => Some(state.available_points.unwrap() > 0),
                        Err(e) => {
                            godot_error!("failed to lock karma state: {}", e.to_string());
                            None
                        }
                    };

                    if let Some(has_points) = has_points {
                        if !has_points {
                            godot_print!("not enough points!")
                        } else {
                            let user = user.first().unwrap();
                            give_karma(user.identity.clone());
                            let msg = MessageHelper::make_karma_given_successfully(
                                user.name.clone().unwrap(),
                            );
                            let sender = self.sender.clone();
                            match sender.send(msg).await {
                                Ok(_) => {}
                                Err(err) => {
                                    godot_error!("receiver was dropped: {}", err.to_string())
                                }
                            }
                        }
                    } else {
                        continue;
                    }
                }
                Payload::Teardown => match state.try_lock() {
                    Err(err) => {
                        godot_error!("receiver was dropped: {}", err.to_string())
                    }
                    Ok(mut v) => {
                        client::Karma::remove_on_insert(v.karma_insert_id.take().unwrap());
                        remove_on_subscription_applied(v.karma_init_id.take().unwrap());
                        v.initialized = false;
                    }
                },
                _ => {}
            }
        }
    }
}

pub async fn run_karma_worker(
    worker: KarmaWorker,
    receiver: broadcast::Receiver<Payload>,
    state: Arc<Mutex<KarmaState>>,
) {
    worker.run(receiver, state).await
}

use spacetimedb_sdk::{SubscriptionCallbackId, table::InsertCallbackId};
use crate::client::Karma;

pub struct KarmaState {
    pub initialized: bool,
    pub karma: Option<u32>,
    pub available_points: Option<u32>,
    pub karma_init_id: Option<SubscriptionCallbackId>,
    pub karma_insert_id: Option<InsertCallbackId<Karma>>,
}

impl KarmaState {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Default for KarmaState {
    fn default() -> Self {
        Self {
            initialized: false,
            karma: None,
            available_points: None,
            karma_init_id: None,
            karma_insert_id: None,
        }
    }
}

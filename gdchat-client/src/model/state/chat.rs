use crate::client;
use spacetimedb_sdk::{
    identity::ConnectCallbackId,
    table::{InsertCallbackId, UpdateCallbackId},
    DisconnectCallbackId, SubscriptionCallbackId,
};

pub struct ChatState {
    pub connected: bool,
    pub online_users: Option<Vec<String>>,
    pub username: Option<String>,

    // callback ids
    pub once_on_connect_id: Option<ConnectCallbackId>,
    pub on_connect_id: Option<ConnectCallbackId>,
    pub on_disconnect_id: Option<DisconnectCallbackId>,
    pub on_insert_user: Option<InsertCallbackId<client::User>>,
    pub on_update_user: Option<UpdateCallbackId<client::User>>,
    pub on_insert_message: Option<InsertCallbackId<client::Message>>,
    pub message_subscription_id: Option<SubscriptionCallbackId>,
    pub user_subscription_id: Option<SubscriptionCallbackId>,
}

impl ChatState {
    pub fn new() -> Self {
        Self::default()
    }
}

impl Default for ChatState {
    fn default() -> Self {
        Self {
            connected: false,
            username: None,
            online_users: None,
            once_on_connect_id: None,
            on_connect_id: None,
            on_disconnect_id: None,
            on_insert_user: None,
            on_update_user: None,
            on_insert_message: None,
            message_subscription_id: None,
            user_subscription_id: None,
        }
    }
}

use super::super::state::chat::ChatState;
use super::super::state::karma::KarmaState;
use std::sync::{Arc, Mutex};

#[derive(Clone)]
pub struct StateManager {
    pub chat_state: Arc<Mutex<ChatState>>,
    pub karma_state: Arc<Mutex<KarmaState>>,
}

impl StateManager {
    pub fn new() -> Self {
        Self {
            chat_state: Arc::new(Mutex::new(ChatState::new())),
            karma_state: Arc::new(Mutex::new(KarmaState::new())),
        }
    }
}

use std::sync::{Arc, Mutex};
use tokio::sync::{mpsc, broadcast};
use godot::prelude::godot_error;
use super::super::state::chat::ChatState;
use super::super::transition::transition::Transition;
use super::super::transition::helper::TransitionHelper;
use super::super::message::message::{Message, MessageType};

pub struct ChatReducer {
    sender: mpsc::Sender<Transition>,
    state: Arc<Mutex<ChatState>>,
}

impl ChatReducer {
    pub fn new(sender: mpsc::Sender<Transition>, state: Arc<Mutex<ChatState>>) -> Self {
        Self { sender, state }
    }

    pub async fn run(&self, mut receiver: broadcast::Receiver<Message>) {
        while let Ok(msg) = receiver.recv().await {
            match msg.msg_type {
                MessageType::ChatSetup {
                    once_on_connect_id,
                    on_connect_id,
                    on_disconnect_id,
                    on_insert_user,
                    on_update_user,
                    on_insert_message,
                    message_subscription_id,
                    user_subscription_id,
                } => match self.state.try_lock() {
                    Err(e) => {
                        godot_error!("failed to lock chat state: {}", e.to_string());
                    }
                    Ok(mut v) => {
                        v.once_on_connect_id = Some(once_on_connect_id);
                        v.on_connect_id = Some(on_connect_id);
                        v.on_disconnect_id = Some(on_disconnect_id);
                        v.on_insert_user = Some(on_insert_user);
                        v.on_update_user = Some(on_update_user);
                        v.on_insert_message = Some(on_insert_message);
                        v.message_subscription_id = Some(message_subscription_id);
                        v.user_subscription_id = Some(user_subscription_id);
                    }
                },
                MessageType::ChatConnectionSuccessful(username) => {
                    let success = match self.state.try_lock() {
                        Ok(mut v) => {
                            v.connected = true;
                            v.username = Some(username.clone());
                            true
                        }
                        Err(e) => {
                            godot_error!("failed to lock chat state: {}", e.to_string());
                            false
                        }
                    };

                    if success {
                        let tx = TransitionHelper::make_chat_has_connected(username);
                        match self.sender.send(tx).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                    }
                }
                MessageType::ChatInitOnlineUsers(online_users) => {
                    let success = match self.state.try_lock() {
                        Err(e) => {
                            godot_error!("failed to lock chat state: {}", e.to_string());
                            false
                        }
                        Ok(mut v) => {
                            v.online_users = Some(online_users.clone());
                            true
                        }
                    };

                    if success {
                        let tx = TransitionHelper::make_chat_got_online_users(online_users);
                        match self.sender.send(tx).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                    }
                }
                MessageType::ChatUsernameUpdateSuccessful(username) => {
                    match self.state.try_lock() {
                        Err(e) => godot_error!("failed to lock chat state: {}", e.to_string()),
                        Ok(mut v) => {
                            v.username = Some(username.clone());
                        }
                    }

                    let tx = TransitionHelper::make_chat_username_was_updated(username);
                    match self.sender.send(tx).await {
                        Ok(_) => {}
                        Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                    }
                }
                MessageType::ChatUserRenamed { oldname, newname } => {
                    let success = match self.state.try_lock() {
                        Err(e) => {
                            godot_error!("failed to lock chat state: {}", e.to_string());
                            false
                        }
                        Ok(mut v) => {
                            match v
                                .online_users
                                .as_ref()
                                .unwrap()
                                .iter()
                                .position(|x| *x == oldname)
                            {
                                None => {}
                                Some(idx) => {
                                    v.online_users.as_mut().unwrap().remove(idx);
                                    v.online_users.as_mut().unwrap().push(newname.clone());
                                }
                            }

                            true
                        }
                    };

                    if success {
                        let tx =
                            TransitionHelper::make_chat_user_has_been_renamed(oldname, newname);
                        match self.sender.send(tx).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                    }
                }
                MessageType::ChatUserJoined(username) => {
                    let success = match self.state.try_lock() {
                        Err(e) => {
                            godot_error!("failed to lock chat state: {}", e.to_string());
                            false
                        }
                        Ok(mut v) => {
                            v.username = Some(username.clone());
                            true
                        }
                    };

                    if success {
                        let tx = TransitionHelper::make_chat_user_has_joined(username);
                        match self.sender.send(tx).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                    }
                }
                MessageType::ChatRemoteUserJoined(username) => {
                    let success = match self.state.try_lock() {
                        Err(e) => {
                            godot_error!("failed to lock chat state: {}", e.to_string());
                            false
                        }
                        Ok(mut v) => {
                            match v.online_users.as_mut() {
                                None => v.online_users = Some(vec![username.clone()]),
                                Some(users) => users.push(username.clone()),
                            }

                            true
                        }
                    };

                    if success {
                        let tx = TransitionHelper::make_chat_remote_user_has_joined(username);
                        match self.sender.send(tx).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                    }
                }
                MessageType::ChatRemoteUserLeaved(username) => {
                    let success = match self.state.try_lock() {
                        Err(e) => {
                            godot_error!("failed to lock chat state: {}", e.to_string());
                            false
                        }
                        Ok(mut v) => {
                            if let Some(_) = v.online_users {
                                match v
                                    .online_users
                                    .as_ref()
                                    .unwrap()
                                    .iter()
                                    .position(|x| *x == username)
                                {
                                    None => {}
                                    Some(idx) => {
                                        v.online_users.as_mut().unwrap().remove(idx);
                                    }
                                }
                            }

                            true
                        }
                    };

                    if success {
                        let tx = TransitionHelper::make_chat_remote_user_has_leaved(username);
                        match self.sender.send(tx).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                    }
                }
                MessageType::ChatDisconnected => {
                    let success = match self.state.try_lock() {
                        Ok(mut v) => {
                            v.connected = false;
                            true
                        }
                        Err(e) => {
                            godot_error!("failed to lock chat state: {}", e.to_string());
                            false
                        }
                    };

                    if success {
                        let tx = TransitionHelper::make_chat_has_disconnected();
                        match self.sender.send(tx).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                    }
                }
                _ => {}
            }
        }
    }
}

pub async fn run_chat_reducer(reducer: ChatReducer, receiver: broadcast::Receiver<Message>) {
    reducer.run(receiver).await
}

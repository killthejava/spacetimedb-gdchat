use std::sync::{Arc, Mutex};
use tokio::sync::{mpsc, broadcast};
use godot::prelude::godot_error;
use super::super::state::karma::KarmaState;
use super::super::transition::transition::Transition;
use super::super::transition::helper::TransitionHelper;
use super::super::message::message::{Message, MessageType};

pub struct KarmaReducer {
    sender: mpsc::Sender<Transition>,
    state: Arc<Mutex<KarmaState>>,
}

impl KarmaReducer {
    pub fn new(sender: mpsc::Sender<Transition>, state: Arc<Mutex<KarmaState>>) -> Self {
        Self { sender, state }
    }

    pub async fn run(&self, mut receiver: broadcast::Receiver<Message>) {
        while let Ok(msg) = receiver.recv().await {
            match msg.msg_type {
                MessageType::KarmaSetup {
                    karma_init_id,
                    karma_insert_id,
                } => match self.state.try_lock() {
                    Err(e) => godot_error!("failed to lock karma state: {}", e.to_string()),
                    Ok(mut state) => {
                        state.karma_init_id = Some(karma_init_id);
                        state.karma_insert_id = Some(karma_insert_id)
                    }
                },
                MessageType::KarmaInitPoints {
                    total_karma,
                    available_points,
                } => {
                    let state = match self.state.try_lock() {
                        Ok(mut state) => {
                            state.initialized = true;
                            state.karma = Some(total_karma);
                            state.available_points = Some(available_points);
                            Some((state.karma.unwrap(), state.available_points.unwrap()))
                        }
                        Err(e) => {
                            godot_error!("failed to lock karma state: {}", e.to_string());
                            None
                        }
                    };

                    if let Some((total_karma, available_points)) = state {
                        let tx = TransitionHelper::make_karma_got_initialized(
                            total_karma,
                            available_points,
                        );
                        match self.sender.send(tx).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was droppped: {}", e.to_string()),
                        }
                    }
                }
                MessageType::KarmaReceived(from) => {
                    let karma = match self.state.try_lock() {
                        Ok(mut state) => {
                            if !state.initialized {
                                godot_error!("received karma but state is not initialized");
                                continue;
                            }

                            state.karma = Some(state.karma.unwrap() + 1);
                            Some(state.karma.unwrap())
                        }
                        Err(e) => {
                            godot_error!("failed to lock karma state: {}", e.to_string());
                            None
                        }
                    };

                    if let Some(karma) = karma {
                        let tx = TransitionHelper::make_karma_has_increased(from, karma);
                        match self.sender.send(tx).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                    }
                }
                MessageType::KarmaGivenSuccessfully(_) => {
                    let available_points = match self.state.try_lock() {
                        Ok(mut state) => {
                            if !state.initialized {
                                godot_error!("gave karma but state is not initialized");
                                continue;
                            }

                            state.available_points = Some(state.available_points.unwrap() - 1);
                            Some(state.available_points.unwrap())
                        }
                        Err(e) => {
                            godot_error!("failed to lock karma state: {}", e.to_string());
                            None
                        }
                    };

                    if let Some(available_points) = available_points {
                        let tx = TransitionHelper::make_karma_available_points_got_updated(
                            available_points,
                        );
                        match self.sender.send(tx).await {
                            Ok(_) => {}
                            Err(e) => godot_error!("receiver was dropped: {}", e.to_string()),
                        }
                    }
                }
                _ => {}
            }
        }
    }
}

pub async fn run_karma_reducer(reducer: KarmaReducer, receiver: broadcast::Receiver<Message>) {
    reducer.run(receiver).await
}

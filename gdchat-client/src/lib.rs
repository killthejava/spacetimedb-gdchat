use godot::prelude::*;

pub(crate) mod chat;
pub(crate) mod client;
pub(crate) mod model;
pub(crate) mod scene;

struct ChatClient;

#[gdextension]
unsafe impl ExtensionLibrary for ChatClient {}

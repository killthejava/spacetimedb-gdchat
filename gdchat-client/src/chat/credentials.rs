use godot::{
    engine::{FileAccess, file_access::ModeFlags},
    prelude::PackedByteArray,
};
use spacetimedb_sdk::{identity::Credentials, spacetimedb_lib::bsatn};

// found in ~/.local/share/godot/app_userdata/gdchat-godot
/// The filename to store SpacetimeDB credentials
const CREDS_PATH: &'static str = "user://.dbcredentials";

/// It persists the user credentials using Godot's FileAccess API
pub fn save_credentials(creds: &Credentials) -> Result<(), bsatn::ser::BsatnError> {
    let creds_bytes = bsatn::to_vec(creds)?;
    let mut file_access = FileAccess::open(CREDS_PATH.into(), ModeFlags::WRITE).unwrap();
    let bytes = PackedByteArray::from(&creds_bytes[..]);
    file_access.store_buffer(bytes);
    file_access.close();

    Ok(())
}

/// It tries to load the user credentials using Godot's FileAccess API
pub fn load_credentials() -> Result<Option<Credentials>, String> {
    if let Some(fs) = FileAccess::open(CREDS_PATH.into(), ModeFlags::READ) {
        let size = fs.get_length();
        let data = fs.get_buffer(size as i64);
        return match bsatn::from_slice::<Credentials>(data.as_slice()) {
            Ok(creds) => Ok(Some(creds)),
            Err(e) => Err(e.to_string()),
        };
    }

    Ok(None)
}

/// It verifies that credentials were saved previously
pub fn check_credentials() -> bool {
    FileAccess::file_exists(CREDS_PATH.into())
}

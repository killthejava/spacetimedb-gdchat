#[derive(Debug, PartialEq)]
pub enum ChatCommand {
    /// Allows users to change their names
    ChangeUsername(String),
}

/// Command used for changing username
const COMMAND_CHANGE_USERNAME: &'static str = "/name";

/// Returns an Option containing the requested command or None otherwise
pub fn get_command(message: &str) -> Option<ChatCommand> {
    let cmd = message.trim_matches(char::is_whitespace);
    if cmd.starts_with(COMMAND_CHANGE_USERNAME) {
        let start = COMMAND_CHANGE_USERNAME.len();
        if start == cmd.len() {
            return None;
        }
        let cmd = &cmd[start..];
        let v: Vec<&str> = cmd.split_terminator(char::is_whitespace).collect();
        return match v.iter().position(|&r| r != "") {
            None => None,
            Some(idx) => match idx {
                0 => None,
                _ => {
                    let username = v[idx].trim_start_matches(char::is_whitespace);
                    match username.find(|c: char| !c.is_ascii_alphanumeric()) {
                        None => Some(ChatCommand::ChangeUsername(username.into())),
                        Some(_) => None,
                    }
                }
            },
        };
    }
    None
}

#[cfg(test)]
mod tests {
    use crate::chat::command::{get_command, ChatCommand};

    #[test]
    fn test_change_username() {
        let cmd = "lolol";
        assert_eq!(get_command(cmd), None);

        let cmd = " lolol";
        assert_eq!(get_command(cmd), None);

        let cmd = "/name";
        assert_eq!(get_command(cmd), None);

        let cmd = " /name";
        assert_eq!(get_command(cmd), None);

        let cmd = "/name ";
        assert_eq!(get_command(cmd), None);

        let cmd = " /name ";
        assert_eq!(get_command(cmd), None);

        let cmd = "/name xyz";
        assert_eq!(
            get_command(cmd),
            Some(ChatCommand::ChangeUsername("xyz".to_string()))
        );

        let cmd = "/name x*yz";
        assert_eq!(get_command(cmd), None);

        let cmd = " /name xyz";
        assert_eq!(
            get_command(cmd),
            Some(ChatCommand::ChangeUsername("xyz".to_string()))
        );

        let cmd = " /name xyz ";
        assert_eq!(
            get_command(cmd),
            Some(ChatCommand::ChangeUsername("xyz".to_string()))
        );

        let cmd = " /name    xyz ";
        assert_eq!(
            get_command(cmd),
            Some(ChatCommand::ChangeUsername("xyz".to_string()))
        );

        let cmd = "/name x[b]gotcha[/b]z";
        assert_eq!(get_command(cmd), None);

        let cmd = " /name x[b]gotcha[/b]z ";
        assert_eq!(get_command(cmd), None);
    }
}

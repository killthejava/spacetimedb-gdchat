# spacetimedb-gdchat #

![](https://gitlab.com/killthejava/spacetimedb-gdchat/raw/master/gdchat.png)

## About ##

`spacetimedb-gdchat` is a Godot chat application made using [SpacetimeDB](https://spacetimedb.com/) Rust SDK, [Godot](https://godotengine.org/) and [gdext](https://github.com/godot-rust/gdext) to glue all together.

At its core, this app is a rewrite of SpacetimeDB [Rust Quickstart](https://spacetimedb.com/docs/sdks/rust/quickstart) but implemented as a Rust extension, so it can be used within a Godot application thanks to [GDExtension](https://docs.godotengine.org/en/stable/tutorials/scripting/gdextension/what_is_gdextension.html).

## Setup ##

This a 3 part repo containing the server code, client code and Godot application. This setup consist in running a local instance of SpacetimeDB, publish the database, compiling the extension and running the Godot app. You can download Godot from [here](https://godotengine.org/download). The Rust compiler can be installed using [rustup](https://rustup.rs/).

### SpacetimeDB  ###

Install [SpacetimeDB](https://spacetimedb.com/). Once installed, you can run a local instance by doing:

```
 $ spacetime start
```

Move to the folder containing the repo. Publish the database to your `local` instance:

```
 $ spacetime publish --project-path gdchat-server --server local gdchat
```

### Client ###

Move to the `gdchat-client` folder. Compile it.

```
 $ cd gdchat-client
 $ cargo build
```

The Godot app pulls the binaries from the `dev` target.

### Godot ###

This application was made using Godot 4.2, so make sure you have a compatible version. Start Godot and open the project located at `gdchat-godot`. Select the `MainScene` node and check that the exported values for `Server` and `Database` correspond to the instance of SpacetimeDB you are trying to connect.

Now start the app. Once up, click the `Connect` button to check if everything went ok.

## Features currently supported ##

 - Can connect to SpacetimeDB instance. Check `MainScene` exported variables from Godot Editor to setup URL and database.
 - Can persist connection credentials to disk.
 - Support for `/name` command for changing username.

## License ##

Released under MIT Licence. Fonts are from the [SG Font Collection](https://spicygame.itch.io/fonts) and are released under CC0.

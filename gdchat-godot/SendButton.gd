extends Button


func _on_transition_emitter_chat_has_connected(_username):
	self.disabled = false

func _on_transition_emitter_chat_has_disconnected():
	self.disabled = true

func _on_pressed():
	var content = %TextEdit.text.strip_edges(true, true)
	if content != "":
		%PayloadController.do_send_message(content)
		%TextEdit.text = ""

func _on_text_edit_gui_input(event):
	if event is InputEventKey \
	and event.is_pressed() \
	and event.keycode == KEY_ENTER \
	and %TextEdit.text.strip_edges(true, true) != "":
		%PayloadController.do_send_message(%TextEdit.text.strip_edges(true, true))
		%TextEdit.text = ""

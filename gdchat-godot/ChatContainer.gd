extends TabContainer

func _ready():
	self.set_tab_title(0, "Chat")

# Returns escaped BBCode that won't be parsed by RichTextLabel as tags.
func escape_bbcode(bbcode_text):
	# We only need to replace opening brackets to prevent tags from being parsed.
	return bbcode_text.replace("[", "[lb]")

func _on_transition_emitter_chat_has_disconnected():
	for child in %MessageContainer.get_children():
		child.queue_free()

func generate_message_content(username, message):
	var content = "[color=#305070]%s[/color]: " % [username]
	content += escape_bbcode(message)
	return content
	
func generate_notification_content(message):
	var content = "[color=#ccc]%s[/color]" % [message]
	return content

func push_message(username, message):
	var label = RichTextLabel.new()
	label.bbcode_enabled = true
	label.fit_content = true
	label.size_flags_vertical = Control.SIZE_SHRINK_BEGIN
	label.text = generate_message_content(username, message)
	%MessageContainer.add_child(label)
	return label

func push_notification(message):
	var label = RichTextLabel.new()
	label.bbcode_enabled = true
	label.fit_content = true
	label.size_flags_vertical = Control.SIZE_SHRINK_BEGIN
	label.text = generate_notification_content(message)
	%MessageContainer.add_child(label)
	return label

func scroll_to_bottom():
	var children = %MessageContainer.get_children().size()
	var last_child = %MessageContainer.get_child(children - 1)
	%ChatScroll.ensure_control_visible(last_child)

func scroll_to(node):
	%ChatScroll.ensure_control_visible(node)
	
func _on_message_emitter_chat_message_posted(username, message):
	var node = push_message(username, message)
	await get_tree().process_frame
	scroll_to(node)

func _on_message_emitter_chat_init_message_list(usernames, messages):
	for i in range(usernames.size()):
		push_message(usernames[i], messages[i])
	if usernames.size() > 0:
		await get_tree().process_frame
		scroll_to_bottom()

func _on_transition_emitter_chat_remote_user_has_joined(username):
	var message = "User %s has joined" % [username]
	var node = push_notification(message)
	await get_tree().process_frame
	scroll_to(node)

func _on_transition_emitter_chat_remote_user_has_leaved(username):
	var message = "User %s leaved" % [username]
	var node = push_notification(message)
	await get_tree().process_frame
	scroll_to(node)

extends RichTextLabel

func _on_message_emitter_chat_connection_failed(err):
	self.text = "Connection failed: %s" % [err]

func _on_transition_emitter_chat_has_disconnected():
	self.text = "You were disconnected from the server"

func _on_transition_emitter_chat_username_was_updated(username):
	self.text = "You are now connected as [color=#9147ff]%s[/color]" % [username]

func _on_transition_emitter_chat_user_has_joined(username):
	self.text = "You are now connected as [color=#9147ff]%s[/color]" % [username]

extends RichTextLabel

const ORIGINAL_TEXT = "Click [b]Connect[/b] to start chatting!"

func _ready():
	self.text = ORIGINAL_TEXT

func _on_transition_emitter_chat_has_online_users(online_users):
	var total = online_users.size()
	self.text = "There are %d users online. Say hi!" % [total]

func _on_transition_emitter_chat_has_disconnected():
	self.text = ORIGINAL_TEXT

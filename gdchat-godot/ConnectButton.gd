extends Button

var connected = false

func _on_message_emitter_chat_connecting():
	self.disabled = true


func _on_message_emitter_chat_connection_failed(_err):
	self.disabled = false


func _on_pressed():
	if connected:
		%PayloadController.do_disconnect()
	else:
		%PayloadController.do_connect()

func _on_message_emitter_chat_disconnecting():
	self.disabled = true


func _on_transition_emitter_chat_has_connected(_username):
	connected = true
	self.text = "Disconnect"
	self.disabled = false


func _on_transition_emitter_chat_has_disconnected():
	connected = false
	self.text = "Connect"
	self.disabled = false

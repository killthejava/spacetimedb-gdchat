extends TextEdit


func _on_transition_emitter_chat_has_connected(_username):
	self.editable = true

func _on_transition_emitter_chat_has_disconnected():
	self.text = ""
	self.editable = false

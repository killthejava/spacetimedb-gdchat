use spacetimedb::{spacetimedb, Identity, ReducerContext, Timestamp};

#[spacetimedb(table)]
pub struct User {
    #[primarykey]
    identity: Identity,
    name: Option<String>,
    karma: u32,
    available_points: u32,
    online: bool,
}

#[spacetimedb(table)]
pub struct Message {
    sender: Identity,
    receiver: Option<Identity>,
    sent: Timestamp,
    text: String,
}

#[spacetimedb(table)]
pub struct Karma {
    sender: Identity,
    receiver: Identity,
    sent: Timestamp,
}

/// Takes a name and checks if it's acceptable as a user's name.
fn validate_name(name: String) -> Result<String, String> {
    if name.is_empty() {
        Err("Names must not be empty".to_string())
    } else {
        Ok(name)
    }
}

#[spacetimedb(reducer)]
/// Clientss invoke this reducer to set their user names.
pub fn set_name(ctx: ReducerContext, name: String) -> Result<(), String> {
    let name = validate_name(name)?;
    if let Some(user) = User::filter_by_identity(&ctx.sender) {
        User::update_by_identity(
            &ctx.sender,
            User {
                name: Some(name),
                ..user
            },
        );
        Ok(())
    } else {
        Err("Cannot set name for unknown user".to_string())
    }
}

/// Takes a message's text and checks if it's acceptable to send.
fn validate_message(text: String) -> Result<String, String> {
    if text.is_empty() {
        Err("Messages must not be empty".to_string())
    } else {
        Ok(text)
    }
}

#[spacetimedb(reducer)]
/// Clients invoke this reducer to send messages.
pub fn send_message(
    ctx: ReducerContext,
    text: String,
    receiver: Option<Identity>,
) -> Result<(), String> {
    let text = validate_message(text)?;
    log::info!("{}", text);
    Message::insert(Message {
        sender: ctx.sender,
        receiver,
        text,
        sent: ctx.timestamp,
    });
    Ok(())
}

#[spacetimedb(reducer)]
pub fn give_karma(ctx: ReducerContext, receiver: Identity) -> Result<(), String> {
    let result = match (
        User::filter_by_identity(&ctx.sender),
        User::filter_by_identity(&receiver),
    ) {
        (Some(sender), Some(recv)) => {
            if sender.available_points == 0 {
                return Err("user does not have any points to give".into());
            }

            let karma = recv.karma + 1;
            User::update_by_identity(&receiver, User { karma, ..recv });

            let available_points = sender.available_points - 1;
            User::update_by_identity(
                &ctx.sender,
                User {
                    available_points,
                    ..sender
                },
            );

            Karma::insert(Karma {
                sender: ctx.sender,
                receiver: receiver,
                sent: ctx.timestamp,
            });

            return Ok(());
        }
        _ => Err("could not identify user".into()),
    };

    result
}

#[spacetimedb(connect)]
// Called when a client connects to the SpacetimeDB
pub fn identity_connected(ctx: ReducerContext) {
    if let Some(user) = User::filter_by_identity(&ctx.sender) {
        // If this is a returning user, i.e. we already have a `User` with this `Identity`,
        // set `online: true`, but leave `name` and `identity` unchanged.
        User::update_by_identity(
            &ctx.sender,
            User {
                online: true,
                ..user
            },
        );
    } else {
        // If this is a new user, create a `User` row for the `Identity`,
        // which is online, but hasn't set a name.
        User::insert(User {
            name: None,
            identity: ctx.sender,
            online: true,
            karma: 0,
            available_points: 5,
        })
        .unwrap();
    }
}

#[spacetimedb(disconnect)]
// Called when a client disconnects from SpacetimeDB
pub fn identity_disconnected(ctx: ReducerContext) {
    if let Some(user) = User::filter_by_identity(&ctx.sender) {
        User::update_by_identity(
            &ctx.sender,
            User {
                online: false,
                ..user
            },
        );
    } else {
        // This branch should be unreachable,
        // as it doesn't make sense for a client to disconnect without connecting first.
        log::warn!(
            "Disconnect event for unknown user with identity {:?}",
            ctx.sender
        );
    }
}
